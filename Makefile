output: main.o
	g++ main.o -o output -pthread -lleveldb -std=c++11
	@echo "============="

main.o: main.cpp
	g++ -c main.cpp

clean:
	rm -rf *o output
