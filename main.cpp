
#include <cassert>
#include <iostream>
#include <string>
#include <leveldb/db.h>
//g++ -o demo testdb.cpp -pthread -lleveldb -std=c++11
//./demo
using namespace std;

int main()
{
  leveldb::DB* db;
  leveldb::Options options;
  options.create_if_missing = true;
  leveldb::Status status = leveldb::DB::Open(options, "./db", &db);
  assert(status.ok());

  string key ="test";
  string name ="name";
  string get;

  db->Put(leveldb::WriteOptions(), "test", "test_value ");
  leveldb::Status s = db->Put(leveldb::WriteOptions(), "name", "ansarul mullah");
  if (s.ok())
	s = db->Get(leveldb::ReadOptions(), key, &get);
	s = db->Get(leveldb::ReadOptions(), name, &get);
  if (s.ok()){
    cout << name<<" : "<< get << std::endl;
	cout << key<<" : "<< get << std::endl;
	}
  else
	cout << "failed to find the key!" << std::endl;

  delete db;

  return 0;
}
